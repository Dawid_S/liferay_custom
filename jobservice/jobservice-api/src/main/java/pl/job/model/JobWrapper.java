/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package pl.job.model;

import aQute.bnd.annotation.ProviderType;

import com.liferay.expando.kernel.model.ExpandoBridge;

import com.liferay.portal.kernel.model.ModelWrapper;
import com.liferay.portal.kernel.service.ServiceContext;

import java.io.Serializable;

import java.util.HashMap;
import java.util.Map;
import java.util.Objects;

/**
 * <p>
 * This class is a wrapper for {@link Job}.
 * </p>
 *
 * @author Brian Wing Shun Chan
 * @see Job
 * @generated
 */
@ProviderType
public class JobWrapper implements Job, ModelWrapper<Job> {
	public JobWrapper(Job job) {
		_job = job;
	}

	@Override
	public Class<?> getModelClass() {
		return Job.class;
	}

	@Override
	public String getModelClassName() {
		return Job.class.getName();
	}

	@Override
	public Map<String, Object> getModelAttributes() {
		Map<String, Object> attributes = new HashMap<String, Object>();

		attributes.put("jobId", getJobId());
		attributes.put("name", getName());
		attributes.put("type", getType());

		return attributes;
	}

	@Override
	public void setModelAttributes(Map<String, Object> attributes) {
		Integer jobId = (Integer)attributes.get("jobId");

		if (jobId != null) {
			setJobId(jobId);
		}

		String name = (String)attributes.get("name");

		if (name != null) {
			setName(name);
		}

		String type = (String)attributes.get("type");

		if (type != null) {
			setType(type);
		}
	}

	@Override
	public boolean isCachedModel() {
		return _job.isCachedModel();
	}

	@Override
	public boolean isEscapedModel() {
		return _job.isEscapedModel();
	}

	@Override
	public boolean isNew() {
		return _job.isNew();
	}

	@Override
	public ExpandoBridge getExpandoBridge() {
		return _job.getExpandoBridge();
	}

	@Override
	public com.liferay.portal.kernel.model.CacheModel<pl.job.model.Job> toCacheModel() {
		return _job.toCacheModel();
	}

	@Override
	public int compareTo(pl.job.model.Job job) {
		return _job.compareTo(job);
	}

	/**
	* Returns the job ID of this job.
	*
	* @return the job ID of this job
	*/
	@Override
	public int getJobId() {
		return _job.getJobId();
	}

	/**
	* Returns the primary key of this job.
	*
	* @return the primary key of this job
	*/
	@Override
	public int getPrimaryKey() {
		return _job.getPrimaryKey();
	}

	@Override
	public int hashCode() {
		return _job.hashCode();
	}

	@Override
	public Serializable getPrimaryKeyObj() {
		return _job.getPrimaryKeyObj();
	}

	@Override
	public java.lang.Object clone() {
		return new JobWrapper((Job)_job.clone());
	}

	/**
	* Returns the name of this job.
	*
	* @return the name of this job
	*/
	@Override
	public java.lang.String getName() {
		return _job.getName();
	}

	/**
	* Returns the type of this job.
	*
	* @return the type of this job
	*/
	@Override
	public java.lang.String getType() {
		return _job.getType();
	}

	@Override
	public java.lang.String toString() {
		return _job.toString();
	}

	@Override
	public java.lang.String toXmlString() {
		return _job.toXmlString();
	}

	@Override
	public pl.job.model.Job toEscapedModel() {
		return new JobWrapper(_job.toEscapedModel());
	}

	@Override
	public pl.job.model.Job toUnescapedModel() {
		return new JobWrapper(_job.toUnescapedModel());
	}

	@Override
	public void persist() {
		_job.persist();
	}

	@Override
	public void setCachedModel(boolean cachedModel) {
		_job.setCachedModel(cachedModel);
	}

	@Override
	public void setExpandoBridgeAttributes(ExpandoBridge expandoBridge) {
		_job.setExpandoBridgeAttributes(expandoBridge);
	}

	@Override
	public void setExpandoBridgeAttributes(
		com.liferay.portal.kernel.model.BaseModel<?> baseModel) {
		_job.setExpandoBridgeAttributes(baseModel);
	}

	@Override
	public void setExpandoBridgeAttributes(ServiceContext serviceContext) {
		_job.setExpandoBridgeAttributes(serviceContext);
	}

	/**
	* Sets the job ID of this job.
	*
	* @param jobId the job ID of this job
	*/
	@Override
	public void setJobId(int jobId) {
		_job.setJobId(jobId);
	}

	/**
	* Sets the name of this job.
	*
	* @param name the name of this job
	*/
	@Override
	public void setName(java.lang.String name) {
		_job.setName(name);
	}

	@Override
	public void setNew(boolean n) {
		_job.setNew(n);
	}

	/**
	* Sets the primary key of this job.
	*
	* @param primaryKey the primary key of this job
	*/
	@Override
	public void setPrimaryKey(int primaryKey) {
		_job.setPrimaryKey(primaryKey);
	}

	@Override
	public void setPrimaryKeyObj(Serializable primaryKeyObj) {
		_job.setPrimaryKeyObj(primaryKeyObj);
	}

	/**
	* Sets the type of this job.
	*
	* @param type the type of this job
	*/
	@Override
	public void setType(java.lang.String type) {
		_job.setType(type);
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}

		if (!(obj instanceof JobWrapper)) {
			return false;
		}

		JobWrapper jobWrapper = (JobWrapper)obj;

		if (Objects.equals(_job, jobWrapper._job)) {
			return true;
		}

		return false;
	}

	@Override
	public Job getWrappedModel() {
		return _job;
	}

	@Override
	public boolean isEntityCacheEnabled() {
		return _job.isEntityCacheEnabled();
	}

	@Override
	public boolean isFinderCacheEnabled() {
		return _job.isFinderCacheEnabled();
	}

	@Override
	public void resetOriginalValues() {
		_job.resetOriginalValues();
	}

	private final Job _job;
}