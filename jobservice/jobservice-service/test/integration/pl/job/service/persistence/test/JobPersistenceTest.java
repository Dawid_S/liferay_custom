/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package pl.job.service.persistence.test;

import com.liferay.arquillian.extension.junit.bridge.junit.Arquillian;

import com.liferay.portal.kernel.dao.orm.ActionableDynamicQuery;
import com.liferay.portal.kernel.dao.orm.DynamicQuery;
import com.liferay.portal.kernel.dao.orm.DynamicQueryFactoryUtil;
import com.liferay.portal.kernel.dao.orm.ProjectionFactoryUtil;
import com.liferay.portal.kernel.dao.orm.QueryUtil;
import com.liferay.portal.kernel.dao.orm.RestrictionsFactoryUtil;
import com.liferay.portal.kernel.test.rule.AggregateTestRule;
import com.liferay.portal.kernel.test.util.RandomTestUtil;
import com.liferay.portal.kernel.transaction.Propagation;
import com.liferay.portal.kernel.util.IntegerWrapper;
import com.liferay.portal.kernel.util.OrderByComparator;
import com.liferay.portal.kernel.util.OrderByComparatorFactoryUtil;
import com.liferay.portal.test.rule.LiferayIntegrationTestRule;
import com.liferay.portal.test.rule.PersistenceTestRule;
import com.liferay.portal.test.rule.TransactionalTestRule;

import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.ClassRule;
import org.junit.Rule;
import org.junit.Test;

import org.junit.runner.RunWith;

import pl.job.exception.NoSuchJobException;

import pl.job.model.Job;

import pl.job.service.JobLocalServiceUtil;
import pl.job.service.persistence.JobPersistence;
import pl.job.service.persistence.JobUtil;

import java.io.Serializable;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 * @generated
 */
@RunWith(Arquillian.class)
public class JobPersistenceTest {
	@ClassRule
	@Rule
	public static final AggregateTestRule aggregateTestRule = new AggregateTestRule(new LiferayIntegrationTestRule(),
			PersistenceTestRule.INSTANCE,
			new TransactionalTestRule(Propagation.REQUIRED, "pl.job.service"));

	@Before
	public void setUp() {
		_persistence = JobUtil.getPersistence();

		Class<?> clazz = _persistence.getClass();

		_dynamicQueryClassLoader = clazz.getClassLoader();
	}

	@After
	public void tearDown() throws Exception {
		Iterator<Job> iterator = _jobs.iterator();

		while (iterator.hasNext()) {
			_persistence.remove(iterator.next());

			iterator.remove();
		}
	}

	@Test
	public void testCreate() throws Exception {
		int pk = RandomTestUtil.nextInt();

		Job job = _persistence.create(pk);

		Assert.assertNotNull(job);

		Assert.assertEquals(job.getPrimaryKey(), pk);
	}

	@Test
	public void testRemove() throws Exception {
		Job newJob = addJob();

		_persistence.remove(newJob);

		Job existingJob = _persistence.fetchByPrimaryKey(newJob.getPrimaryKey());

		Assert.assertNull(existingJob);
	}

	@Test
	public void testUpdateNew() throws Exception {
		addJob();
	}

	@Test
	public void testUpdateExisting() throws Exception {
		int pk = RandomTestUtil.nextInt();

		Job newJob = _persistence.create(pk);

		newJob.setName(RandomTestUtil.randomString());

		newJob.setType(RandomTestUtil.randomString());

		_jobs.add(_persistence.update(newJob));

		Job existingJob = _persistence.findByPrimaryKey(newJob.getPrimaryKey());

		Assert.assertEquals(existingJob.getJobId(), newJob.getJobId());
		Assert.assertEquals(existingJob.getName(), newJob.getName());
		Assert.assertEquals(existingJob.getType(), newJob.getType());
	}

	@Test
	public void testFindByPrimaryKeyExisting() throws Exception {
		Job newJob = addJob();

		Job existingJob = _persistence.findByPrimaryKey(newJob.getPrimaryKey());

		Assert.assertEquals(existingJob, newJob);
	}

	@Test(expected = NoSuchJobException.class)
	public void testFindByPrimaryKeyMissing() throws Exception {
		int pk = RandomTestUtil.nextInt();

		_persistence.findByPrimaryKey(pk);
	}

	@Test
	public void testFindAll() throws Exception {
		_persistence.findAll(QueryUtil.ALL_POS, QueryUtil.ALL_POS,
			getOrderByComparator());
	}

	protected OrderByComparator<Job> getOrderByComparator() {
		return OrderByComparatorFactoryUtil.create("job", "jobId", true,
			"name", true, "type", true);
	}

	@Test
	public void testFetchByPrimaryKeyExisting() throws Exception {
		Job newJob = addJob();

		Job existingJob = _persistence.fetchByPrimaryKey(newJob.getPrimaryKey());

		Assert.assertEquals(existingJob, newJob);
	}

	@Test
	public void testFetchByPrimaryKeyMissing() throws Exception {
		int pk = RandomTestUtil.nextInt();

		Job missingJob = _persistence.fetchByPrimaryKey(pk);

		Assert.assertNull(missingJob);
	}

	@Test
	public void testFetchByPrimaryKeysWithMultiplePrimaryKeysWhereAllPrimaryKeysExist()
		throws Exception {
		Job newJob1 = addJob();
		Job newJob2 = addJob();

		Set<Serializable> primaryKeys = new HashSet<Serializable>();

		primaryKeys.add(newJob1.getPrimaryKey());
		primaryKeys.add(newJob2.getPrimaryKey());

		Map<Serializable, Job> jobs = _persistence.fetchByPrimaryKeys(primaryKeys);

		Assert.assertEquals(2, jobs.size());
		Assert.assertEquals(newJob1, jobs.get(newJob1.getPrimaryKey()));
		Assert.assertEquals(newJob2, jobs.get(newJob2.getPrimaryKey()));
	}

	@Test
	public void testFetchByPrimaryKeysWithMultiplePrimaryKeysWhereNoPrimaryKeysExist()
		throws Exception {
		int pk1 = RandomTestUtil.nextInt();

		int pk2 = RandomTestUtil.nextInt();

		Set<Serializable> primaryKeys = new HashSet<Serializable>();

		primaryKeys.add(pk1);
		primaryKeys.add(pk2);

		Map<Serializable, Job> jobs = _persistence.fetchByPrimaryKeys(primaryKeys);

		Assert.assertTrue(jobs.isEmpty());
	}

	@Test
	public void testFetchByPrimaryKeysWithMultiplePrimaryKeysWhereSomePrimaryKeysExist()
		throws Exception {
		Job newJob = addJob();

		int pk = RandomTestUtil.nextInt();

		Set<Serializable> primaryKeys = new HashSet<Serializable>();

		primaryKeys.add(newJob.getPrimaryKey());
		primaryKeys.add(pk);

		Map<Serializable, Job> jobs = _persistence.fetchByPrimaryKeys(primaryKeys);

		Assert.assertEquals(1, jobs.size());
		Assert.assertEquals(newJob, jobs.get(newJob.getPrimaryKey()));
	}

	@Test
	public void testFetchByPrimaryKeysWithNoPrimaryKeys()
		throws Exception {
		Set<Serializable> primaryKeys = new HashSet<Serializable>();

		Map<Serializable, Job> jobs = _persistence.fetchByPrimaryKeys(primaryKeys);

		Assert.assertTrue(jobs.isEmpty());
	}

	@Test
	public void testFetchByPrimaryKeysWithOnePrimaryKey()
		throws Exception {
		Job newJob = addJob();

		Set<Serializable> primaryKeys = new HashSet<Serializable>();

		primaryKeys.add(newJob.getPrimaryKey());

		Map<Serializable, Job> jobs = _persistence.fetchByPrimaryKeys(primaryKeys);

		Assert.assertEquals(1, jobs.size());
		Assert.assertEquals(newJob, jobs.get(newJob.getPrimaryKey()));
	}

	@Test
	public void testActionableDynamicQuery() throws Exception {
		final IntegerWrapper count = new IntegerWrapper();

		ActionableDynamicQuery actionableDynamicQuery = JobLocalServiceUtil.getActionableDynamicQuery();

		actionableDynamicQuery.setPerformActionMethod(new ActionableDynamicQuery.PerformActionMethod<Job>() {
				@Override
				public void performAction(Job job) {
					Assert.assertNotNull(job);

					count.increment();
				}
			});

		actionableDynamicQuery.performActions();

		Assert.assertEquals(count.getValue(), _persistence.countAll());
	}

	@Test
	public void testDynamicQueryByPrimaryKeyExisting()
		throws Exception {
		Job newJob = addJob();

		DynamicQuery dynamicQuery = DynamicQueryFactoryUtil.forClass(Job.class,
				_dynamicQueryClassLoader);

		dynamicQuery.add(RestrictionsFactoryUtil.eq("jobId", newJob.getJobId()));

		List<Job> result = _persistence.findWithDynamicQuery(dynamicQuery);

		Assert.assertEquals(1, result.size());

		Job existingJob = result.get(0);

		Assert.assertEquals(existingJob, newJob);
	}

	@Test
	public void testDynamicQueryByPrimaryKeyMissing() throws Exception {
		DynamicQuery dynamicQuery = DynamicQueryFactoryUtil.forClass(Job.class,
				_dynamicQueryClassLoader);

		dynamicQuery.add(RestrictionsFactoryUtil.eq("jobId",
				RandomTestUtil.nextInt()));

		List<Job> result = _persistence.findWithDynamicQuery(dynamicQuery);

		Assert.assertEquals(0, result.size());
	}

	@Test
	public void testDynamicQueryByProjectionExisting()
		throws Exception {
		Job newJob = addJob();

		DynamicQuery dynamicQuery = DynamicQueryFactoryUtil.forClass(Job.class,
				_dynamicQueryClassLoader);

		dynamicQuery.setProjection(ProjectionFactoryUtil.property("jobId"));

		Object newJobId = newJob.getJobId();

		dynamicQuery.add(RestrictionsFactoryUtil.in("jobId",
				new Object[] { newJobId }));

		List<Object> result = _persistence.findWithDynamicQuery(dynamicQuery);

		Assert.assertEquals(1, result.size());

		Object existingJobId = result.get(0);

		Assert.assertEquals(existingJobId, newJobId);
	}

	@Test
	public void testDynamicQueryByProjectionMissing() throws Exception {
		DynamicQuery dynamicQuery = DynamicQueryFactoryUtil.forClass(Job.class,
				_dynamicQueryClassLoader);

		dynamicQuery.setProjection(ProjectionFactoryUtil.property("jobId"));

		dynamicQuery.add(RestrictionsFactoryUtil.in("jobId",
				new Object[] { RandomTestUtil.nextInt() }));

		List<Object> result = _persistence.findWithDynamicQuery(dynamicQuery);

		Assert.assertEquals(0, result.size());
	}

	protected Job addJob() throws Exception {
		int pk = RandomTestUtil.nextInt();

		Job job = _persistence.create(pk);

		job.setName(RandomTestUtil.randomString());

		job.setType(RandomTestUtil.randomString());

		_jobs.add(_persistence.update(job));

		return job;
	}

	private List<Job> _jobs = new ArrayList<Job>();
	private JobPersistence _persistence;
	private ClassLoader _dynamicQueryClassLoader;
}