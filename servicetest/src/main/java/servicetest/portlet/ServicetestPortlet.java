package servicetest.portlet;

import servicetest.constants.ServicetestPortletKeys;

import com.liferay.counter.kernel.service.CounterLocalServiceUtil;
import com.liferay.expando.kernel.model.ExpandoBridge;
import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.model.BaseModel;
import com.liferay.portal.kernel.model.CacheModel;
import com.liferay.portal.kernel.portlet.bridges.mvc.MVCPortlet;
import com.liferay.portal.kernel.service.ServiceContext;
import com.liferay.portal.kernel.service.UserLocalServiceUtil;
import com.liferay.portal.kernel.util.ParamUtil;

import pl.job.model.Job;
import pl.job.service.JobLocalServiceUtil;

import java.io.IOException;
import java.io.Serializable;
import java.util.Map;

import javax.portlet.ActionRequest;
import javax.portlet.ActionResponse;
import javax.portlet.Portlet;
import javax.portlet.PortletException;
import javax.portlet.RenderRequest;
import javax.portlet.RenderResponse;

import org.osgi.service.component.annotations.Component;

/**
 * @author liferay
 */
@Component(
	immediate = true,
	property = {
		"com.liferay.portlet.display-category=category.sample",
		"com.liferay.portlet.instanceable=true",
		"javax.portlet.display-name=servicetest Portlet",
		"javax.portlet.init-param.template-path=/",
		"javax.portlet.init-param.view-template=/view.jsp",
		"javax.portlet.name=" + ServicetestPortletKeys.Servicetest,
		"javax.portlet.resource-bundle=content.Language",
		"javax.portlet.security-role-ref=power-user,user"
	},
	service = Portlet.class
)
public class ServicetestPortlet extends MVCPortlet {
	
	public void jobSubmit(ActionRequest request, ActionResponse response) throws IOException, PortletException{
		String name = ParamUtil.getString(request, "name");
		String type = ParamUtil.getString(request, "type");
		try {
			Job job = JobLocalServiceUtil.createJob((int)CounterLocalServiceUtil.increment());
			job.setName(name);
			job.setType(type);
			JobLocalServiceUtil.addJob(job);
		} catch (Exception e) {
			e.printStackTrace();
		}		
	}
	

	@Override
	public void doView(RenderRequest renderRequest, RenderResponse renderResponse)
			throws IOException, PortletException {
		try {
			System.out.println("view ==>" + JobLocalServiceUtil.getJob(1).getName() + "=====>" + UserLocalServiceUtil.getUsersCount());
			
		} catch (PortalException e) {
			e.printStackTrace();
		}
		super.doView(renderRequest, renderResponse);
	}
}